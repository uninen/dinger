# -*- coding: utf-8 -*-
import urllib2
import time

from django.shortcuts import render
from django.conf import settings
from django import forms

class URLForm(forms.Form):
    url = forms.URLField()

class HeadRequest(urllib2.Request):
    def get_method(self):
        return 'HEAD'

def get_head_dict(url):
    req = HeadRequest(url)
    req.add_header('Accept-encoding', 'gzip')
    try:
        # start_time = time.time()
        resp = urllib2.urlopen(req, timeout=15)
        # end_time = time.time()
        # elapsed_time = end_time - start_time
        # print "Urlopen took %s seconds." % elapsed_time
        return resp.info().dict
    except urllib2.URLError:
        return {'error': True}

def index(request):
    hg_revision = settings.HG_REVISION_NUMBER
    hg_last_updated = settings.HG_LAST_UPDATED
    
    if request.method == 'POST':
        form = URLForm(request.POST)
        if form.is_valid():
            url = form.cleaned_data['url']
            start_time = time.time()
            response = get_head_dict(url)
            end_time = time.time()
            elapsed_time = end_time - start_time
            
            gzip_support = response.get('content-encoding', False) == 'gzip'
            etag_support = response.has_key('etag')
            server = response.get('server', 'Unknown')
            timeout = response.has_key('error')
            form = URLForm() 
        else:
            return render(request, 'index.html', locals())
    else:
        form = URLForm()
    
    return render(request, 'index.html', locals())
