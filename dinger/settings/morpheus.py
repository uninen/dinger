# -*- coding: utf-8 -*-
from local_settings import *
from common.hg import get_hg_revision_and_date

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Ville Säävuori', 'ville@syneus.fi'),
)
MANAGERS = ADMINS

MEDIA_URL = '/media/'
STATIC_URL = '/static/'
ADMIN_MEDIA_PREFIX = '/static/admin/'

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

INSTALLED_APPS += (
    'pinger',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

HG_REVISION_NUMBER, HG_LAST_UPDATED = get_hg_revision_and_date('.')