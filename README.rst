======
Dinger
======

Django Pinger. Pings URLs on Web for status codes and response times. Works 
from multiple machines and instances over multiple URLs.